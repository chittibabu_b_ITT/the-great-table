import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators} from '@angular/forms'
import { Router, ActivatedRoute} from '@angular/router'
import { UserdataService } from '../userdata.service';
@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  loginForm : FormGroup;
  localStorageData: {};
  email: string;
  password: string;

  constructor(private route: ActivatedRoute, private router: Router, private userData : UserdataService) { }

  ngOnInit(): void {
    this.loginForm = new FormGroup(
      {
        email : new FormControl('', [Validators.required, Validators.email,Validators.pattern("^[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$")]),
        password : new FormControl('', [Validators.required, Validators.minLength(6)])
      }
    )
  }

  submitLogin(){
    this.email = this.loginForm.get('email').value; 
    this.password  = this.loginForm.get('password').value;

    
    if( this.userData.loginUser(this.email,this.password)) {
      this.router.navigateByUrl('home');
    } else {
      alert("invalid credentials")
    }
  }
  
}
