import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { UserdataService } from '../userdata.service';
@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss'],
})
export class RegisterComponent implements OnInit {
  registrationForm: FormGroup;
  key: string;
  value: string;
  logindata: {};
  password: string;
  passwordVerify: string;

  constructor(private route: Router, private userData : UserdataService) {}

  ngOnInit(): void {
    this.registrationForm = new FormGroup({
      email: new FormControl('', [
        Validators.required,
        Validators.email,
        Validators.pattern('^[a-z0-9._%+-]+@[a-z0-9.-]+.[a-z]{2,4}$'),
      ]),
      password: new FormControl('', [
        Validators.required,
        Validators.minLength(6),
      ]),
      passwordVerify: new FormControl('', [
        Validators.required,
        Validators.minLength(6),
      ]),
    });
  }

  submitRegister() {
    this.key = this.registrationForm.get('email').value;
    this.password = this.registrationForm.get('password').value;
    this.passwordVerify = this.registrationForm.get('passwordVerify').value;
    if (this.password === this.passwordVerify) {
      this.logindata = {
        email: this.key,
        password: this.password,
      };
      if(this.userData.checkUserAlreadyExists(this.key)){
        this.userData.registerUser(this.key,this.logindata)
        alert("User Registered Successfully")
        this.route.navigateByUrl('/login');
      }
      else {
        alert("user already exists")
      }
    }
  }
}
