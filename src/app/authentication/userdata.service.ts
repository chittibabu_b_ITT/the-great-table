import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class UserdataService {
  localStorageData: {};
  isLoggedIn : boolean = false;

  constructor() { }

  registerUser(key: string ,logindata : {}) {
    localStorage.setItem(key, JSON.stringify(logindata));
  }

  loginUser(email:string, password: string){
    this.localStorageData = JSON.parse(localStorage.getItem(email));
    if(this.localStorageData !== null && password === this.localStorageData['password']) {
      this.isLoggedIn = true;
      return true;
    } else {
      return false;
    }
  }

  checkUserAlreadyExists(email:string) {
    this.localStorageData = JSON.parse(localStorage.getItem(email));
    if(this.localStorageData !== null) {
      return false;
    } else {
      return true;
    }
  }

  checkLoginStatus(){
    return this.isLoggedIn;
  }


}
