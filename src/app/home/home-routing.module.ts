import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AuthGuard } from '../auth.guard';
import { ShoppingCartComponent } from '../shopping-cart/shopping-cart.component';

import { HomeComponent } from './home.component';

const routes: Routes = [
  { path: '', component: HomeComponent,

    children: [
      { 
        path: 'shopping-cart', 
        loadChildren: () => import('../shopping-cart/shopping-cart.module').then(m => m.ShoppingCartModule), 
        children: [
          {
            path: '',
            component: ShoppingCartComponent,
            outlet: 'home'
          }
        ]
      },
      { 
        path: '', 
        redirectTo: 'shopping-cart',
        canActivate: [AuthGuard]
      }
    ],
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class HomeRoutingModule { }
