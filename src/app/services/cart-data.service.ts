import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class CartDataService {
  constructor() {}

  shoppingCartList = [];
  newShoppingCartList = [];
  localStorageBS = new Subject();

  set localStorageData(value: any[]) {
    this.localStorageBS.next(value);
    // localStorage.setItem('cart', JSON.stringify(value));
  }

  get localStorageData() {
    // this.newShoppingCartList.push(localStorage.getItem('cart'))
    // return this.newShoppingCartList;
    return this.newShoppingCartList;
  }

  addToList(itemEntry: any) {
    this.shoppingCartList.push(itemEntry);
    this.getShoppingCartList()
    // LOCAL_STORAGE
    // var cartData;
    // var localData;
    // if (localStorage.getItem('cart') === null) {
    //   localData = [
    //     {
    //       name: itemEntry.name,
    //       price: itemEntry.price,
    //       count: 1,
    //     },
    //   ];
    //   this.localStorageData = localData;
    // } else {
    //   cartData = JSON.parse(localStorage.getItem('cart'));
    //   var foundIndex = false;
    //   for (var i = 0; i < cartData.length; i++) {
    //     if (cartData[i].name == itemEntry.name) {
    //       cartData[i].count = parseInt(cartData[i].count) + 1;
    //       foundIndex = true;
    //       break;
    //     }
    //   }
    //   if (!foundIndex) {
    //     localData = {
    //       name: itemEntry.name,
    //       price: itemEntry.price,
    //       count: 1,
    //     };
    //     cartData.push(localData);
    //   }
    //   this.localStorageData = cartData;
    // }
  }

  getShoppingCartList() {
    
    var itemsList = [];
    var counter = {};
    var index = 0;

    this.shoppingCartList.forEach(function (obj) {
      counter[JSON.stringify(obj)] = (counter[JSON.stringify(obj)] || 0) + 1;
    });

    var itemData = Object.keys(counter);
    itemData.forEach(function (obj) {
      itemsList.push(JSON.parse(obj));
    });

    var countData = Object.values(counter);

    countData.forEach(function (obj) {
      itemsList[index].count = obj;
      index++;
    });

    this.localStorageData = itemsList;

  }

  deleteFromList(itemEntry: any) {
    this.shoppingCartList.splice(
      this.shoppingCartList.findIndex((a) => a.name === itemEntry.name),
      1
    );
    this.getShoppingCartList()
    // LOCAL_STORAGE

    // var cartData: any;
    // if (localStorage.getItem('cart') !== null) {
    //   cartData = JSON.parse(localStorage.getItem('cart'));
    //   var foundIndex = false;
    //   for (var i = 0; i < cartData.length; i++) {
    //     if (cartData[i].name == itemEntry.name) {
    //       if (cartData[i].count > 1) {
    //         cartData[i].count = parseInt(cartData[i].count) - 1;
    //         foundIndex = true;
    //         break;
    //       } else {
    //         cartData.splice(
    //           cartData.findIndex((a) => a.name === itemEntry.name),
    //           1
    //         );
    //       }
    //     }
    //   }
    //   this.localStorageData = cartData;
    // }
  }

}
