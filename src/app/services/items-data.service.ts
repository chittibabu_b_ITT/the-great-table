import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root',
})
export class ItemsDataService {
  constructor(private http: HttpClient) {}

  getData(type: string) {
    let url = 'https://www.thecocktaildb.com/api/json/v1/1/filter.php?';
    switch (type) {
      case 'alcoholic':
        url += 'a=Alcoholic';
        break;
      case 'non-alcoholic':
        url += 'a=Non_Alcoholic';
        break;
      case 'ordinary':
        url += 'c=Ordinary_Drink';
        break;
      case 'cocktail':
        url += 'c=Cocktail';
        break;
      case 'cocktail-glass':
        url += 'g=Cocktail_glass';
        break;
      case 'champagne-flute':
        url += 'g=Champagne_flute';
        break;
    }

    return this.http.get(url);
  }
}
