import { Component, OnInit } from '@angular/core';
import { TestBed } from '@angular/core/testing';
import { Observable, Subscription } from 'rxjs';
import { CartDataService } from 'src/app/services/cart-data.service';

@Component({
  selector: 'app-cart',
  templateUrl: './cart.component.html',
  styleUrls: ['./cart.component.scss'],
})
export class CartComponent implements OnInit {
  shoppingList : any;
  unitPrice: number;
  totalPrice:number;
  taxAmount: number;
  finalAmount: number;
  
  constructor(private cartData: CartDataService) {}
  
  ngOnInit(): void {
    this.totalPrice = 0.00
    this.finalAmount = 0.00
    this.cartData.localStorageBS.subscribe(data => {
        this.shoppingList = data;
    })
  }

  getUnitPrice(price,units) {
    this.unitPrice = parseFloat((price*units).toFixed(2));
    return this.unitPrice;
  }

  getTotalPrice() {
    var result :number;
    if(this.shoppingList) {
      result = this.shoppingList.reduce(function(tot, arr) { 
        return parseFloat(tot) + (parseFloat(arr.price) * parseFloat(arr.count));
      },0);
      this.totalPrice = parseFloat(result.toFixed(2));
      this.taxAmount = parseFloat((this.totalPrice/18).toFixed(2))
      this.finalAmount = parseFloat((this.totalPrice + this.taxAmount).toFixed(2))
      return "$"+this.totalPrice;
    }
  }
   
}
