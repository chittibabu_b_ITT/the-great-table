import { Component, OnInit } from '@angular/core';
import { CartDataService } from 'src/app/services/cart-data.service';
import { ItemsDataService } from 'src/app/services/items-data.service';

@Component({
  selector: 'app-catalogue',
  templateUrl: './catalogue.component.html',
  styleUrls: ['./catalogue.component.scss'],
})
export class CatalogueComponent implements OnInit {
  selectionList = [
    'ordinary',
    'alcoholic',
    'non-alcoholic',
    'cocktail',
    'cocktail-glass',
    'champagne-flute',
  ];
  cardData = [];
  selected: string;
  shoppingList = [];

  constructor(
    private itemData: ItemsDataService,
    private cartData: CartDataService
  ) { }

  ngOnInit(): void {
    this.selected = 'ordinary';
    this.getItemsData();
  }

  getItemsData() {
    if (this.selected !== null) {
      // this.getCardDataPrev();
      this.cardData = [];
      this.itemData.getData(this.selected).subscribe((data) => {
        data['drinks'].forEach((element: any) => {
          element['price'] = (parseFloat(element['idDrink']) / 1000).toFixed(2);
          this.cardData.push(element);
        });
      });
    }
  }

  addToShoppingList(id: string, name: string, price: string) {
    var card = document.getElementById(id);
    var value = (parseInt(card.getAttribute("value")) + 1) + '';
    card.setAttribute("value", value);
    card.innerHTML = card.getAttribute("value")+'';
    var selectedItemInfo = {
      id: id,
      name: name,
      price: price,
    };

    this.cartData.addToList(selectedItemInfo);

  }

  removeFromShoppingList(id: string, name: string, price: string) {
    var card = document.getElementById(id);
    var value = parseInt(card.getAttribute("value"))+'';
    if (parseInt(value) > 0) {
      var selectedItemInfo = {
        name: name,
        price: price,
      };
      this.cartData.deleteFromList(selectedItemInfo);
      value = (parseInt(value) - 1) + '';
      card.setAttribute("value", value);
      card.innerHTML = card.getAttribute("value")+'';
    }
  }

  // getCardDataPrev() {
  //   this.cartData.localStorageBS.subscribe((data) => {
  //     data.forEach(element => {
  //       console.log(element.count)
  //       if(element.count > 0) {
  //         document.getElementById(element.id).value = element.count;
  //         document.getElementById(element.id).innerHTML = element.count;
  //       }
  //     });
  //   });
  // }
}
