import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ShoppingCartRoutingModule } from './shopping-cart-routing.module';
import { ShoppingCartComponent } from './shopping-cart.component';
import { HeaderComponent } from './header/header.component';
import { CatalogueComponent } from './catalogue/catalogue.component';
import { CartComponent } from './cart/cart.component';
import { NgSelectModule } from '@ng-select/ng-select';
import { FormsModule } from '@angular/forms';


@NgModule({
  declarations: [ShoppingCartComponent, HeaderComponent, CatalogueComponent, CartComponent],
  imports: [
    CommonModule,
    ShoppingCartRoutingModule,
    NgSelectModule,
    FormsModule
  ]
})
export class ShoppingCartModule { }
